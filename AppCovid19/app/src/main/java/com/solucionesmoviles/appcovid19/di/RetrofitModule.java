package com.solucionesmoviles.appcovid19.di;

import com.solucionesmoviles.appcovid19.interfaces.Ciudadano;
import com.solucionesmoviles.appcovid19.interfaces.Pais;
import com.solucionesmoviles.appcovid19.interfaces.TipoDocumentoIdentidad;
import com.solucionesmoviles.appcovid19.tools.Constants;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class RetrofitModule {
    @Singleton
    @Provides
    GsonConverterFactory provGsonConverterFactory() {
        return GsonConverterFactory.create();
    }

    @Singleton
    @Provides
    HttpLoggingInterceptor provideHttpLogginInterceptor() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.level(HttpLoggingInterceptor.Level.BODY);

        return  loggingInterceptor;
    }

    @Singleton
    @Provides
    OkHttpClient provideOkHttpClient(HttpLoggingInterceptor loggingInterceptor) {
        return  new OkHttpClient.Builder().addInterceptor(loggingInterceptor).build();
    }

    @Singleton
    @Provides
    RxJava2CallAdapterFactory provideRxJava2CallAdapaterFactory() {
        return RxJava2CallAdapterFactory.create();
    }

    @Singleton
    @Provides
    Retrofit provideRetrofit(OkHttpClient httpClient, GsonConverterFactory gsonConverterFactory, RxJava2CallAdapterFactory rxJava2CallAdapterFactory) {
        return  new Retrofit.Builder()
                .baseUrl(Constants.END_POINT)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(rxJava2CallAdapterFactory)
                .client(httpClient)
                .build();

    }

    // ========================================

    @Singleton
    @Provides
    Pais providePais(Retrofit retrofit) {
        return  retrofit.create(Pais.class);
    }

    @Singleton
    @Provides
    TipoDocumentoIdentidad provideTipoDocumentoIdentidad(Retrofit retrofit) {
        return retrofit.create(TipoDocumentoIdentidad.class);
    }

    @Singleton
    @Provides
    Ciudadano provideCiudadano(Retrofit retrofit) {
        return  retrofit.create(Ciudadano.class);
    }
}

