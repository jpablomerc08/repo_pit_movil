package com.solucionesmoviles.appcovid19.models;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.Serializable;

public class TipoDocumentoIdentidadBean implements Serializable {
    private int tipoDocumentoIdentidadId;
    private String descripcionLarga;
    private String descripcionCorta;

    public int getTipoDocumentoIdentidadId() {
        return tipoDocumentoIdentidadId;
    }

    public void setTipoDocumentoIdentidadId(int tipoDocumentoIdentidadId) {
        this.tipoDocumentoIdentidadId = tipoDocumentoIdentidadId;
    }

    public String getDescripcionLarga() {
        return descripcionLarga;
    }

    public void setDescripcionLarga(String descripcionLarga) {
        this.descripcionLarga = descripcionLarga;
    }

    public String getDescripcionCorta() {
        return descripcionCorta;
    }

    public void setDescripcionCorta(String descripcionCorta) {
        this.descripcionCorta = descripcionCorta;
    }

    @NonNull
    @Override
    public String toString() {
        return getDescripcionCorta();
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof TipoDocumentoIdentidadBean) {
            TipoDocumentoIdentidadBean bean = (TipoDocumentoIdentidadBean)obj;
            if (bean.getDescripcionCorta().equals(descripcionCorta))
                return  true;
        }
        return false;
    }
}
