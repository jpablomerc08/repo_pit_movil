package com.solucionesmoviles.appcovid19.dal;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import kotlin.jvm.internal.PropertyReference0Impl;

public class DataProvider extends ContentProvider {
    private static final int PAIS = 100;
    private static final int PAIS_ID = 101;
    private static final int DEPARTAMENTO = 200;
    private static final int DEPARTAMENTO_ID = 201;
    private static final int PROVINCIA = 300;
    private static final int PROVINCIA_ID = 301;
    private static final int DISTRITO = 400;
    private static final int DISTRITO_ID = 401;
    private static final int TIPODOCUMENTOIDENTIDAD = 500;
    private static final int TIPODOCUMENTOIDENTIDAD_ID = 501;
    private static final int CIUDADANO = 600;
    private static final int CIUDADANO_ID = 601;
    private static final int CONTACTO = 700;
    private static final int CONTACTO_ID = 701;
    private static final int DIRECCION = 800;
    private static final int DIRECCION_ID = 801;

    // Objeto UriMatcher para comprobar el Content Uri
    private static final UriMatcher sUriMatcher = buildUriMatcher();
    private DbHelper dbHelper;

    public static UriMatcher buildUriMatcher() {
        String content = DataContract.CONTENT_AUTHORITY;

        UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI(content, DataContract.PATH_PAIS, PAIS);
        matcher.addURI(content, DataContract.PATH_PAIS + "/#", PAIS_ID);

        matcher.addURI(content, DataContract.PATH_DEPARTAMENTO, DEPARTAMENTO);
        matcher.addURI(content, DataContract.PATH_DEPARTAMENTO + "/#", DEPARTAMENTO_ID);

        matcher.addURI(content, DataContract.PATH_PROVINCIA, PROVINCIA);
        matcher.addURI(content, DataContract.PATH_PROVINCIA + "/#", PROVINCIA_ID);

        matcher.addURI(content, DataContract.PATH_DISTRITO, DISTRITO);
        matcher.addURI(content, DataContract.PATH_DISTRITO + "/#", DISTRITO_ID);

        matcher.addURI(content, DataContract.PATH_TIPODOCUMENTOIDENTIDAD, TIPODOCUMENTOIDENTIDAD);
        matcher.addURI(content, DataContract.PATH_TIPODOCUMENTOIDENTIDAD + "/#", TIPODOCUMENTOIDENTIDAD_ID);

        return matcher;
    }

    @Override
    public boolean onCreate() {
        dbHelper = new DbHelper(getContext());
        return true;
    }

    // Realiza la solicitud para la Uri. Necesita projection, selection, selection arguments, and sort ordeer
    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        // Base de datos en modo lectura
        final SQLiteDatabase db = dbHelper.getReadableDatabase();

        // Cursor con el resultado de la solicitud
        Cursor cursor;

        int match = sUriMatcher.match(uri);
        switch (match) {
            case PAIS:
                cursor = db.query(
                        DataContract.PaisEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            case PAIS_ID:
                selection = DataContract.PaisEntry._ID + " = ?";
                selectionArgs = new String[] {String.valueOf(ContentUris.parseId(uri))};

                cursor = db.query(
                        DataContract.PaisEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;

            case DEPARTAMENTO:
                cursor = db.query(
                        DataContract.DepartamentoEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;

            case DEPARTAMENTO_ID:
                selection = DataContract.DepartamentoEntry._ID + " = ?";
                selectionArgs = new String[] {String.valueOf(ContentUris.parseId(uri))};

                cursor = db.query(
                        DataContract.DepartamentoEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;

            case PROVINCIA:
                cursor = db.query(
                        DataContract.ProvinciaEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;

            case PROVINCIA_ID:
                selection = DataContract.ProvinciaEntry._ID + " = ?";
                selectionArgs = new String[] {String.valueOf(ContentUris.parseId(uri))};

                cursor = db.query(
                        DataContract.ProvinciaEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;

            case DISTRITO:
                cursor = db.query(
                        DataContract.DistritoEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;

            case DISTRITO_ID:
                selection = DataContract.DistritoEntry._ID + " = ?";
                selectionArgs = new String[] {String.valueOf(ContentUris.parseId(uri))};

                cursor = db.query(
                        DataContract.DistritoEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;

            case TIPODOCUMENTOIDENTIDAD:
                cursor = db.query(
                        DataContract.TipoDocumentoIdentidadEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;

            case TIPODOCUMENTOIDENTIDAD_ID:
                selection = DataContract.TipoDocumentoIdentidadEntry._ID + " = ?";
                selectionArgs = new String[] {String.valueOf(ContentUris.parseId(uri))};

                cursor = db.query(
                        DataContract.TipoDocumentoIdentidadEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;

            case CIUDADANO:
                cursor = db.query(
                        DataContract.CiudadanoEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;

            case CIUDADANO_ID:
                selection = DataContract.CiudadanoEntry._ID + " = ?";
                selectionArgs = new String[] {String.valueOf(ContentUris.parseId(uri))};

                cursor = db.query(
                        DataContract.CiudadanoEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;

            case CONTACTO:
                cursor = db.query(
                        DataContract.ContactoEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;

            case CONTACTO_ID:
                selection = DataContract.ContactoEntry._ID + " = ?";
                selectionArgs = new String[] {String.valueOf(ContentUris.parseId(uri))};

                cursor = db.query(
                        DataContract.ContactoEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;

            case DIRECCION:
                cursor = db.query(
                        DataContract.DireccionEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;

            case DIRECCION_ID:
                selection = DataContract.DireccionEntry._ID + " = ?";
                selectionArgs = new String[] {String.valueOf(ContentUris.parseId(uri))};

                cursor = db.query(
                        DataContract.DireccionEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;

                default:
                    throw new IllegalArgumentException("Uri desconocida: " + uri);
        }

        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    // Insertar nuevos datos en el provider con los ContentValues
    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        long id;
        Uri returnUri;

        int match = sUriMatcher.match(uri);
        switch (match) {
            case PAIS:
                id = db.insert(DataContract.PaisEntry.TABLE_NAME, null, values);
                if (id > 0) {
                    returnUri = DataContract.PaisEntry.builUrl(id);
                }
                else {
                    Log.d(DataProvider.class.getSimpleName(), "Fallo al insertar datos " + uri);
                    throw new UnsupportedOperationException("No se pudo insertar filas con: " + uri);
                }
                break;

            case DEPARTAMENTO:
                id = db.insert(DataContract.DistritoEntry.TABLE_NAME, null, values);
                if (id > 0) {
                    returnUri = DataContract.DepartamentoEntry.builUrl(id);
                }
                else {
                    Log.d(DataProvider.class.getSimpleName(), "Fallo al insertar datos " + uri);
                    throw new UnsupportedOperationException("No se pudo insertar filas con: " + uri);
                }
                break;

            case PROVINCIA:
                id = db.insert(DataContract.ProvinciaEntry.TABLE_NAME, null, values);
                if (id > 0) {
                    returnUri = DataContract.ProvinciaEntry.builUrl(id);
                }
                else {
                    Log.d(DataProvider.class.getSimpleName(), "Fallo al insertar datos " + uri);
                    throw new UnsupportedOperationException("No se pudo insertar filas con: " + uri);
                }
                break;

            case DISTRITO:
                id = db.insert(DataContract.DistritoEntry.TABLE_NAME, null, values);
                if (id > 0) {
                    returnUri = DataContract.DistritoEntry.builUrl(id);
                }
                else {
                    Log.d(DataProvider.class.getSimpleName(), "Fallo al insertar datos " + uri);
                    throw new UnsupportedOperationException("No se pudo insertar filas con: " + uri);
                }
                break;

            case TIPODOCUMENTOIDENTIDAD:
                id = db.insert(DataContract.TipoDocumentoIdentidadEntry.TABLE_NAME, null, values);
                if (id > 0) {
                    returnUri = DataContract.TipoDocumentoIdentidadEntry.builUrl(id);
                }
                else {
                    Log.d(DataProvider.class.getSimpleName(), "Fallo al insertar datos " + uri);
                    throw new UnsupportedOperationException("No se pudo insertar filas con: " + uri);
                }
                break;

            case CIUDADANO:
                id = db.insert(DataContract.CiudadanoEntry.TABLE_NAME, null, values);
                if (id > 0) {
                    returnUri = DataContract.CiudadanoEntry.builUrl(id);
                }
                else {
                    Log.d(DataProvider.class.getSimpleName(), "Fallo al insertar datos " + uri);
                    throw new UnsupportedOperationException("No se pudo insertar filas con: " + uri);
                }
                break;

            case CONTACTO:
                id = db.insert(DataContract.ContactoEntry.TABLE_NAME, null, values);
                if (id > 0) {
                    returnUri = DataContract.ContactoEntry.builUrl(id);
                }
                else {
                    Log.d(DataProvider.class.getSimpleName(), "Fallo al insertar datos " + uri);
                    throw new UnsupportedOperationException("No se pudo insertar filas con: " + uri);
                }
                break;

            case DIRECCION:
                id = db.insert(DataContract.DireccionEntry.TABLE_NAME, null, values);
                if (id > 0) {
                    returnUri = DataContract.DireccionEntry.builUrl(id);
                }
                else {
                    Log.d(DataProvider.class.getSimpleName(), "Fallo al insertar datos " + uri);
                    throw new UnsupportedOperationException("No se pudo insertar filas con: " + uri);
                }
                break;

                default:
                    throw new UnsupportedOperationException("Uri desconocida: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    // Actualizar los datos con selection y selection argument, con los nuevos ContentValues
    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        int rows;

        int match = sUriMatcher.match(uri);
        switch (match) {
            case PAIS:
                rows = updatePais(uri, values, selection, selectionArgs);
                break;

            case PAIS_ID:
                selection = DataContract.PaisEntry._ID + " = ?";
                selectionArgs = new String[] {String.valueOf(ContentUris.parseId(uri))};
                rows = updatePais(uri, values, selection, selectionArgs);
                break;

            case DEPARTAMENTO:
                rows = db.update(DataContract.DepartamentoEntry.TABLE_NAME, values, selection, selectionArgs);
                break;

            case DEPARTAMENTO_ID:
                selection = DataContract.DepartamentoEntry._ID + " = ?";
                selectionArgs = new String[] {String.valueOf(ContentUris.parseId(uri))};
                rows = db.update(DataContract.DepartamentoEntry.TABLE_NAME, values, selection, selectionArgs);
                break;

            case DISTRITO:
                rows = db.update(DataContract.DistritoEntry.TABLE_NAME, values, selection, selectionArgs);
                break;

            case DISTRITO_ID:
                selection = DataContract.DistritoEntry._ID + " = ?";
                selectionArgs = new String[] {String.valueOf(ContentUris.parseId(uri))};
                rows = db.update(DataContract.DistritoEntry.TABLE_NAME, values, selection, selectionArgs);
                break;

            case TIPODOCUMENTOIDENTIDAD:
                rows = db.update(DataContract.TipoDocumentoIdentidadEntry.TABLE_NAME, values, selection, selectionArgs);
                break;

            case TIPODOCUMENTOIDENTIDAD_ID:
                selection = DataContract.TipoDocumentoIdentidadEntry._ID + "=?";
                selectionArgs = new String[] {String.valueOf(ContentUris.parseId(uri))};
                rows = db.update(DataContract.TipoDocumentoIdentidadEntry.TABLE_NAME, values, selection, selectionArgs);
                break;

            case CIUDADANO:
                rows = db.update(DataContract.CiudadanoEntry.TABLE_NAME, values, selection, selectionArgs);
                break;

            case CIUDADANO_ID:
                selection = DataContract.CiudadanoEntry._ID + "=?";
                selectionArgs = new String[] {String.valueOf(ContentUris.parseId(uri))};
                rows = db.update(DataContract.CiudadanoEntry.TABLE_NAME, values, selection, selectionArgs);
                break;

            case CONTACTO:
                rows = db.update(DataContract.ContactoEntry.TABLE_NAME, values, selection, selectionArgs);
                break;

            case CONTACTO_ID:
                selection = DataContract.ContactoEntry._ID + "=?";
                selectionArgs = new String[] {String.valueOf(ContentUris.parseId(uri))};
                rows = db.update(DataContract.ContactoEntry.TABLE_NAME, values, selection, selectionArgs);
                break;

            case DIRECCION:
                rows = db.update(DataContract.DireccionEntry.TABLE_NAME, values, selection, selectionArgs);
                break;

            case DIRECCION_ID:
                selection = DataContract.DireccionEntry._ID + "=?";
                selectionArgs = new String[] {String.valueOf(ContentUris.parseId(uri))};
                rows = db.update(DataContract.DireccionEntry.TABLE_NAME, values, selection, selectionArgs);
                break;

            default:
                throw new UnsupportedOperationException("Uri desconocida: " + uri);
        }

        if (rows > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return rows;
    }

    // Actualización personalizada para gestionar reglas
    private int updatePais(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        if (values.containsKey(DataContract.PaisEntry.COLUMN_CODIGOISO)) {
            String codigoISOAux = values.getAsString(DataContract.PaisEntry.COLUMN_CODIGOISO);
            if (codigoISOAux == null) {
                throw new IllegalArgumentException("Se requiere código ISO");
            }
        }

        if (values.containsKey(DataContract.PaisEntry.COLUMN_NOMBRE)) {
            String nombreAux = values.getAsString(DataContract.PaisEntry.COLUMN_NOMBRE);
            if (nombreAux == null) {
                throw  new IllegalArgumentException("Se requiere nombre");
            }
        }

        // Base de datos en modo Escritura
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        // Retornar el número de filas afectadas
        return db.update(DataContract.PaisEntry.TABLE_NAME, values, selection, selectionArgs);
    }

    // Eliminar los datos con selection y selection arguments
    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        // Base de datos en modo Escritura
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        int rows;

        int match = sUriMatcher.match(uri);
        switch (match) {
            case PAIS:
                rows = db.delete(DataContract.PaisEntry.TABLE_NAME, selection, selectionArgs);
                break;

            case DEPARTAMENTO:
                rows = db.delete(DataContract.DepartamentoEntry.TABLE_NAME, selection, selectionArgs);
                break;

            case PROVINCIA:
                rows = db.delete(DataContract.ProvinciaEntry.TABLE_NAME, selection, selectionArgs);
                break;

            case DISTRITO:
                rows = db.delete(DataContract.DistritoEntry.TABLE_NAME, selection, selectionArgs);
                break;

            case TIPODOCUMENTOIDENTIDAD:
                rows = db.delete(DataContract.TipoDocumentoIdentidadEntry.TABLE_NAME, selection, selectionArgs);
                break;

            case CIUDADANO:
                rows = db.delete(DataContract.CiudadanoEntry.TABLE_NAME, selection, selectionArgs);
                break;

            case CONTACTO:
                rows = db.delete(DataContract.ContactoEntry.TABLE_NAME, selection, selectionArgs);
                break;

            case DIRECCION:
                rows = db.delete(DataContract.DireccionEntry.TABLE_NAME, selection, selectionArgs);
                break;

            default:
                throw new IllegalArgumentException("La eliminación no correcta para " + uri);
        }

        // Sí la selección es nula, podría eliminar todas las filas de la tabla
        if (selection == null || rows != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return rows;
    }

    // Devuelve MIME Type de los datos utilizados
    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        int match = sUriMatcher.match(uri);
        switch (match) {
            case PAIS:
                return  DataContract.PaisEntry.CONTENT_TYPE;
            case PAIS_ID:
                return DataContract.PaisEntry.CONTENT_ITEM_TYPE;

            case DEPARTAMENTO:
                return DataContract.DepartamentoEntry.CONTENT_TYPE;
            case DEPARTAMENTO_ID:
                return DataContract.DepartamentoEntry.CONTENT_ITEM_TYPE;

            case PROVINCIA:
                return DataContract.ProvinciaEntry.CONTENT_TYPE;
            case PROVINCIA_ID:
                return DataContract.ProvinciaEntry.CONTENT_ITEM_TYPE;

            case DISTRITO:
                return DataContract.DistritoEntry.CONTENT_TYPE;
            case DISTRITO_ID:
                return DataContract.DistritoEntry.CONTENT_ITEM_TYPE;

            case TIPODOCUMENTOIDENTIDAD:
                return DataContract.TipoDocumentoIdentidadEntry.CONTENT_TYPE;
            case TIPODOCUMENTOIDENTIDAD_ID:
                return DataContract.TipoDocumentoIdentidadEntry.CONTENT_ITEM_TYPE;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }
}
