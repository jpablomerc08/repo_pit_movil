package com.solucionesmoviles.appcovid19;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Context;
import android.content.Intent;
import android.location.SettingInjectorService;
import android.os.Bundle;
import android.view.View;

public class AccesosActivity extends AppCompatActivity {
    private Context context;
    private androidx.appcompat.widget.Toolbar toolbar;
    private AppCompatButton btn_siguiente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accesos);

        setTitle(R.string.title_accesos);

        context = getApplicationContext();
        toolbar = findViewById(R.id.toolbar);
        btn_siguiente = findViewById(R.id.btn_siguiente);

        setSupportActionBar(toolbar);

        addEventListener();
    }

    private void addEventListener() {
        btn_siguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, MainActivity.class);
                startActivity(i);
            }
        });
    }
}
