package com.solucionesmoviles.appcovid19.dal;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.solucionesmoviles.appcovid19.tools.Constants;

public class DbHelper extends SQLiteOpenHelper {
    public DbHelper(Context context) {
        super(context, Constants.DATABASE_NAME, null, Constants.BD_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        addPaisTable(db);
        addDepartamentoTable(db);
        addProvinciaTable(db);
        addDistritoTable(db);
        addTipoDocumentoIdentidadTable(db);
        addCiudadanoTable(db);
        addContactoTable(db);
        addDireccionTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private void addPaisTable(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + DataContract.PaisEntry.TABLE_NAME + "(" +
                DataContract.PaisEntry._ID + " INTEGER PRIMARY KEY NOT NULL," +
                DataContract.PaisEntry.COLUMN_CODIGOISO + " CHAR(2) NOT NULL," +
                DataContract.PaisEntry.COLUMN_NOMBRE + " VARCHAR(100) NOT NULL" +
                ");";

        db.execSQL(sql);
    }

    private void addDepartamentoTable(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + DataContract.DepartamentoEntry.TABLE_NAME + "(" +
                DataContract.DepartamentoEntry._ID + " INTEGER PRIMARY KEY NOT NULL," +
                DataContract.DepartamentoEntry.COLUMN_UBIGEO + " CHAR(2) NOT NULL," +
                DataContract.DepartamentoEntry.COLUMN_NOMBRE + " VARCHAR(50) NOT NULL" +
                ");";

        db.execSQL(sql);
    }

    private void addProvinciaTable(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + DataContract.ProvinciaEntry.TABLE_NAME + "(" +
                DataContract.ProvinciaEntry._ID + " INTEGER PRIMARY KEY NOT NULL, " +
                DataContract.ProvinciaEntry.COLUMN_UBIGEO + " CHAR(4) NOT NULL, " +
                DataContract.ProvinciaEntry.COLUMN_NOMBRE + " VARCHAR(100) NOT NULL, " +
                DataContract.ProvinciaEntry.COLUMN_DEPARTAMENTOID + " INTEGER NOT NULL, " +
                "FOREIGN KEY (" + DataContract.ProvinciaEntry.COLUMN_DEPARTAMENTOID + ") " +
                "REFERENCES " + DataContract.DepartamentoEntry.TABLE_NAME + " (" + DataContract.DepartamentoEntry._ID +
                "));";

        db.execSQL(sql);
    }

    private void addDistritoTable(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + DataContract.DistritoEntry.TABLE_NAME + "(" +
                DataContract.DistritoEntry._ID + " INTEGER PRIMARY KEY NOT NULL, " +
                DataContract.DistritoEntry.COLUMN_UBIGEO + " CHAR(6) NOT NULL, " +
                DataContract.DistritoEntry.COLUMN_NOMBRE + " VARCHAR(100) NOT NULL, " +
                DataContract.DistritoEntry.COLUMN_PROVINCIAID + " INTEGER NOT NULL, " +
                "FOREIGN KEY (" + DataContract.DistritoEntry.COLUMN_PROVINCIAID + ") " +
                "REFERENCES " + DataContract.ProvinciaEntry.TABLE_NAME + " (" + DataContract.ProvinciaEntry._ID +
                "));";

        db.execSQL(sql);
    }

    private void addTipoDocumentoIdentidadTable(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + DataContract.TipoDocumentoIdentidadEntry.TABLE_NAME + "(" +
                DataContract.TipoDocumentoIdentidadEntry._ID + " INTEGER PRIMARY KEY NOT NULL," +
                DataContract.TipoDocumentoIdentidadEntry.COLUMN_DESCRIPCIONLARGA + " VARCHAR(50) NOT NULL," +
                DataContract.TipoDocumentoIdentidadEntry.COLUMN_DESCRIPCIONCORTA + " VARCHAR(20) NOT NULL" +
                ");";

        db.execSQL(sql);
    }

    private void addCiudadanoTable(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + DataContract.CiudadanoEntry.TABLE_NAME + "(" +
                DataContract.CiudadanoEntry._ID + " INTEGER PRIMARY KEY NOT NULL," +
                DataContract.CiudadanoEntry.COLUMN_APELLIDO_PATERNO + " VARCHAR(50)," +
                DataContract.CiudadanoEntry.COLUMN_APELLIDO_MATERNO + " VARCHAR(50)," +
                DataContract.CiudadanoEntry.COLUMN_NOMBRE + " VARCHAR(50)," +
                DataContract.CiudadanoEntry.COLUMN_SEXO + " CHAR(1) NOT NULL," +
                DataContract.CiudadanoEntry.COLUMN_NACIONALIDADID + " INT NOT NULL," +
                DataContract.CiudadanoEntry.COLUMN_TIPODOCUMENTOIDETIDADID + " INT NOT NULL," +
                DataContract.CiudadanoEntry.COLUMN_NRODOCUMENTOIDENTIDAD + " VARCHAR(12) NOT NULL," +
                DataContract.CiudadanoEntry.COLUMN_CELULAR + " CHAR(9)," +
                DataContract.CiudadanoEntry.COLUMN_CORREO + " VARCHAR(100)," +
                DataContract.CiudadanoEntry.COLUMN_FECHANACIMIENTO + " DATE, " +
                "FOREIGN KEY (" + DataContract.CiudadanoEntry.COLUMN_NACIONALIDADID + ") " +
                "REFERENCES " + DataContract.CiudadanoEntry.TABLE_NAME + " (" + DataContract.PaisEntry._ID + "), " +
                "FOREIGN KEY (" + DataContract.CiudadanoEntry.COLUMN_TIPODOCUMENTOIDETIDADID + ") " +
                "REFERENCES " + DataContract.CiudadanoEntry.TABLE_NAME + " (" + DataContract.TipoDocumentoIdentidadEntry._ID + ") " +
                ");";

        db.execSQL(sql);
    }

    private void addContactoTable(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + DataContract.ContactoEntry.TABLE_NAME + "(" +
                DataContract.ContactoEntry._ID + " INTEGER PRIMARY KEY NOT NULL," +
                DataContract.ContactoEntry.COLUMN_CIUDADANOID + " INT NOT NULL," +
                DataContract.ContactoEntry.COLUMN_NOMBRECONTACTO + " VARCHAR(200) NOT NULL," +
                DataContract.ContactoEntry.COLUMN_PARENTESCO + " VARCHAR(150) NOT NULL," +
                DataContract.ContactoEntry.COLUMN_TELEFONO + " CHAR(9)," +
                DataContract.ContactoEntry.COLUMN_CORREO + " VARCHAR(100)," +
                "FOREIGN KEY (" + DataContract.ContactoEntry.COLUMN_CIUDADANOID + ") " +
                "REFERENCES " + DataContract.CiudadanoEntry.TABLE_NAME + " (" + DataContract.CiudadanoEntry._ID + ") " +
                ");";

        db.execSQL(sql);
    }

    private void addDireccionTable(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + DataContract.DireccionEntry.TABLE_NAME + "(" +
                DataContract.DireccionEntry._ID + " INTEGER PRIMARY KEY NOT NULL," +
                DataContract.DireccionEntry.COLUMN_DISTRITOID + " INT NOT NULL," +
                DataContract.DireccionEntry.COLUMN_CIUDADANOID + " INT NOT NULL," +
                DataContract.DireccionEntry.COLUMN_DESCRIPCION + " VARCHAR(200) NOT NULL," +
                "FOREIGN KEY (" + DataContract.DireccionEntry.COLUMN_CIUDADANOID + ") " +
                "REFERENCES " + DataContract.CiudadanoEntry.TABLE_NAME + " (" + DataContract.CiudadanoEntry._ID + ") " +
                ");";

        db.execSQL(sql);
    }
}
