package com.solucionesmoviles.appcovid19;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MensajeActivity extends AppCompatActivity {
    private Context context;
    private AppCompatButton btn_Siguiente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mensaje);

        context = getApplicationContext();

        btn_Siguiente = findViewById(R.id.btn_Siguiente);

        addEventListener();
    }

    private void addEventListener() {
        btn_Siguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, AccesosActivity.class);
                startActivity(i);
            }
        });
    }
}
