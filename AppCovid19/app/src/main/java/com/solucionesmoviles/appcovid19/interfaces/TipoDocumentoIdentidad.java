package com.solucionesmoviles.appcovid19.interfaces;

import com.solucionesmoviles.appcovid19.models.TipoDocumentoIdentidadBean;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface TipoDocumentoIdentidad {
    @GET("TiposDocumentoIdentidad")
    Observable<List<TipoDocumentoIdentidadBean>> listarObservable();
}
