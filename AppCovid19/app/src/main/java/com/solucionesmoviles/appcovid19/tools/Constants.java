package com.solucionesmoviles.appcovid19.tools;

public class Constants {
    // Database
    public static final String PATH = "covid19";
    public static final String DATABASE_NAME = PATH + ".db";
    public static final int BD_VERSION = 1;



    // Services
    public static final String END_POINT = "http://edinsonaldaz-001-site1.htempurl.com/api/";

    public static String FORMATO_FECHA = "ddMMyyyy:HH:mm:ss.SSS";

    // KeysDB
    public static final int PAISID_PERU = 177;
    public static final int TIPO_DOCUMENTO_IDENT_DNI = 1;
    public static final int TIPO_DOCUMENTO_IDENT_CE = 2;
    public static final int TIPO_DOCUMENTO_IDENT_PASS = 3;
}
