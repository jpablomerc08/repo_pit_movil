package com.solucionesmoviles.appcovid19.interfaces;

import com.solucionesmoviles.appcovid19.models.DistritoBean;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Distrito {
    @GET("Distrito/{ProvinciaId}")
    Observable<DistritoBean> listarXProvincia(@Query(value = "ProvinciaId") int provinciaId);
}
