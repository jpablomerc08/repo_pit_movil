package com.solucionesmoviles.appcovid19.interfaces;

import com.solucionesmoviles.appcovid19.models.PaisBean;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface Pais {
    @GET("Paises")
    Observable<List<PaisBean>> listarObservable();
}
