package com.solucionesmoviles.appcovid19.interfaces;

import com.solucionesmoviles.appcovid19.models.CiudadanoBean;

import io.reactivex.Observable;
import retrofit2.http.POST;

public interface Ciudadano {
    @POST("Ciudadanos")
    Observable<CiudadanoBean> grabar(CiudadanoBean entidad);
}
