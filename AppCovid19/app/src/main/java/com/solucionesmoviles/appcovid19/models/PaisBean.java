package com.solucionesmoviles.appcovid19.models;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.Serializable;

public class PaisBean implements Serializable {
    private int paisId;
    private String codigoISO;
    private String nombre;

    public int getPaisId() {
        return paisId;
    }

    public void setPaisId(int paisId) {
        this.paisId = paisId;
    }

    public String getCodigoISO() {
        return codigoISO;
    }

    public void setCodigoISO(String codigoISO) {
        this.codigoISO = codigoISO;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @NonNull
    @Override
    public String toString() {
        return getNombre();
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof PaisBean) {
            PaisBean p = (PaisBean) obj;
            if (p.getNombre().equals(nombre) && p.getPaisId() == paisId)
                return true;
        }
        return false;
    }
}
