package com.solucionesmoviles.appcovid19.interfaces;

import com.solucionesmoviles.appcovid19.models.ProvinciaBean;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Provincia {
    @GET("Pronvincia/{DepartamentoId}")
    Observable<ProvinciaBean> listarXDepartamento(@Query(value = "DepartamentoId") int departamentoId);
}
