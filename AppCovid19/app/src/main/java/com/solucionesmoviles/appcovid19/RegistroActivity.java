package com.solucionesmoviles.appcovid19;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.solucionesmoviles.appcovid19.dal.DataContract;
import com.solucionesmoviles.appcovid19.di.BaseApplication;
import com.solucionesmoviles.appcovid19.interfaces.Pais;
import com.solucionesmoviles.appcovid19.interfaces.TipoDocumentoIdentidad;
import com.solucionesmoviles.appcovid19.models.PaisBean;
import com.solucionesmoviles.appcovid19.models.TipoDocumentoIdentidadBean;
import com.solucionesmoviles.appcovid19.tools.Constants;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class RegistroActivity extends AppCompatActivity {

    private Context context;
    private androidx.appcompat.widget.Toolbar toolbar;
    private RelativeLayout relativeLayout;

    private TextInputLayout til_celular;
    private Spinner cbo_nacionalidad;
    private Spinner cbo_tipoDocumento;
    private TextInputLayout til_nroDocumento;
    private AppCompatEditText ace_celular;
    private AppCompatEditText ace_nroDocumento;
    private AppCompatButton btn_siguiente;

    private Disposable disposable;

    @Inject
    Pais paisClient;
    @Inject
    TipoDocumentoIdentidad tipoDocumentoIdentidadClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        setUpDagger();
        setUpView();
        setUpData();
    }

    private void setUpDagger() {
        ((BaseApplication)getApplication()).getRetrofitComponent().inject(this);
    }

    private void setUpView() {
        try {
            context = getApplicationContext();
            toolbar = findViewById(R.id.toolbar);
            relativeLayout = findViewById(R.id.relativeLayout);

            til_celular = findViewById(R.id.til_celular);
            cbo_nacionalidad = findViewById(R.id.cbo_nacionalidad);
            cbo_tipoDocumento = findViewById(R.id.cbo_tipoDocumento);
            til_nroDocumento = findViewById(R.id.til_nroDocumento);
            ace_celular = findViewById(R.id.ace_celular);
            ace_nroDocumento = findViewById(R.id.ace_nroDocumento);
            btn_siguiente = findViewById(R.id.btn_siguiente);

            relativeLayout.setOnClickListener(null);
            til_celular.setCounterEnabled(true);
            til_celular.setCounterMaxLength(9);
            til_nroDocumento.setCounterEnabled(true);
            til_nroDocumento.setCounterMaxLength(20);

            setSupportActionBar(toolbar);
            setTitle(R.string.title_registro);

            addEventListener();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUpData() {
        String recrearBD = getString(R.string.RecrearDB);
        if (recrearBD.equals("1")) {
            deleteDatabase(Constants.DATABASE_NAME);

            cargarPaises();
            cargarTiposDocumentoIdentidad();
        }

        // Validar si existe el ciudadado(por su dni) para no volver a registrarlo y pasar directamente al activity principal
        /*boolean existe = false;
        if (existe) {
            // Redireccionar al activity main
            Intent i = new Intent(context, MainActivity.class);
            startActivity(i);
        }*/
    }

    private void cargarPaises() {
        paisClient.listarObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<PaisBean>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(List<PaisBean> lista) {
                        for(PaisBean r : lista) {
                            ContentValues values = new ContentValues();
                            values.put(DataContract.PaisEntry._ID, r.getPaisId());
                            values.put(DataContract.PaisEntry.COLUMN_CODIGOISO, r.getCodigoISO());
                            values.put(DataContract.PaisEntry.COLUMN_NOMBRE, r.getNombre());
                            Uri newUri = getContentResolver().insert(DataContract.PaisEntry.CONTENT_URI, values);
                        }

                        cargarComboPaises();
                    }

                    @Override
                    public void onError(Throwable e) {
                        String mensaje = "Error al consumir el servicio " + e.getMessage();
                        Log.d("Paises", mensaje);
                        Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onComplete() {
                        Log.d("Paises", "onComplete");
                    }
                });
    }

    private void cargarTiposDocumentoIdentidad() {
        tipoDocumentoIdentidadClient
                .listarObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<TipoDocumentoIdentidadBean>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(List<TipoDocumentoIdentidadBean> lista) {
                        for(TipoDocumentoIdentidadBean r : lista) {
                            ContentValues values = new ContentValues();
                            values.put(DataContract.TipoDocumentoIdentidadEntry._ID, r.getTipoDocumentoIdentidadId());
                            values.put(DataContract.TipoDocumentoIdentidadEntry.COLUMN_DESCRIPCIONLARGA, r.getDescripcionLarga());
                            values.put(DataContract.TipoDocumentoIdentidadEntry.COLUMN_DESCRIPCIONCORTA, r.getDescripcionCorta());
                            Uri newUri = getContentResolver().insert(DataContract.TipoDocumentoIdentidadEntry.CONTENT_URI, values);

                            System.out.println(newUri);
                        }

                        CargarComboTiposDocumentoIdentidad();
                    }

                    @Override
                    public void onError(Throwable e) {
                        String mensaje = "Error al consumir el servicio " + e.getMessage();
                        Log.d("TiposDocumentoIdentidad", mensaje);
                        Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onComplete() {
                        Log.d("TiposDocumentoIdentidad", "onComplete");
                    }
                });
    }

    private void cargarComboPaises() {
        String[] proyeccion = {
                DataContract.PaisEntry._ID,
                DataContract.PaisEntry.COLUMN_CODIGOISO,
                DataContract.PaisEntry.COLUMN_NOMBRE
        };

        Cursor cursor = getContentResolver().query(DataContract.PaisEntry.CONTENT_URI, proyeccion, null, null, null);
        int idColumnIndex = cursor.getColumnIndex(DataContract.PaisEntry._ID);
        int codigoISOColumnIndex = cursor.getColumnIndex(DataContract.PaisEntry.COLUMN_CODIGOISO);
        int nombreColumnIndex = cursor.getColumnIndex(DataContract.PaisEntry.COLUMN_NOMBRE);

        List<PaisBean> lista = new ArrayList<>();
        PaisBean entidad = null;
        while (cursor.moveToNext()) {
            entidad = new PaisBean();
            entidad.setPaisId(cursor.getInt(idColumnIndex));
            entidad.setCodigoISO(cursor.getString(codigoISOColumnIndex));
            entidad.setNombre(cursor.getString(nombreColumnIndex));

            lista.add(entidad);
        }

        ArrayAdapter<PaisBean> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, lista);
        cbo_nacionalidad.setAdapter(adapter);

        // Seleccionar Perú por defecto
        String[] proyeccionPeru = {
                DataContract.PaisEntry._ID,
                DataContract.PaisEntry.COLUMN_CODIGOISO,
                DataContract.PaisEntry.COLUMN_NOMBRE
        };

        Uri peruUri = ContentUris.withAppendedId(DataContract.PaisEntry.CONTENT_URI, Constants.PAISID_PERU);
        cursor = getContentResolver().query(peruUri, proyeccionPeru, null, null);
        if (cursor.moveToNext()) {
            PaisBean peru = new PaisBean();
            peru.setPaisId(cursor.getInt(0));
            peru.setCodigoISO(cursor.getString(1));
            peru.setNombre(cursor.getString(2));

            cbo_nacionalidad.setSelection(adapter.getPosition(peru));
        }
    }

    private void CargarComboTiposDocumentoIdentidad() {
        String[] proyeccion = {
                DataContract.TipoDocumentoIdentidadEntry._ID,
                DataContract.TipoDocumentoIdentidadEntry.COLUMN_DESCRIPCIONLARGA,
                DataContract.TipoDocumentoIdentidadEntry.COLUMN_DESCRIPCIONCORTA
        };

        Cursor cursor = getContentResolver().query(DataContract.TipoDocumentoIdentidadEntry.CONTENT_URI, proyeccion, null, null, null);
        int idColumnIndex = cursor.getColumnIndex(DataContract.TipoDocumentoIdentidadEntry._ID);
        int descripcionLargaColumnIndex = cursor.getColumnIndex(DataContract.TipoDocumentoIdentidadEntry.COLUMN_DESCRIPCIONLARGA);
        int descripcionCortaColumnIndex = cursor.getColumnIndex(DataContract.TipoDocumentoIdentidadEntry.COLUMN_DESCRIPCIONCORTA);

        List<TipoDocumentoIdentidadBean> lista = new ArrayList<>();
        TipoDocumentoIdentidadBean entidad = null;
        while (cursor.moveToNext()) {
            entidad = new TipoDocumentoIdentidadBean();
            entidad.setTipoDocumentoIdentidadId(cursor.getInt(idColumnIndex));
            entidad.setDescripcionLarga(cursor.getString(descripcionLargaColumnIndex));
            entidad.setDescripcionCorta(cursor.getString(descripcionCortaColumnIndex));

            lista.add(entidad);
        }

        ArrayAdapter<TipoDocumentoIdentidadBean> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, lista);
        cbo_tipoDocumento.setAdapter(adapter);

    }

    private void addEventListener() {
        ace_celular.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                verificarCelular();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        ace_celular.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                verificarCelular();
            }
        });

        btn_siguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_siguiente_click(v);
            }
        });
    }

    private void verificarCelular() {
        if (ace_celular.getText().toString().isEmpty()) {
            til_celular.setErrorEnabled(true);
            til_celular.setError("Ingrese número de celular");
        }
        else {
            til_celular.setErrorEnabled(false);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposable.dispose();
    }

    private void btn_siguiente_click(View v) {
        String celular = ace_celular.getText().toString();
        int paisId = ((PaisBean) cbo_nacionalidad.getSelectedItem()).getPaisId();
        int tipoDocumentoIdentidadId = ((TipoDocumentoIdentidadBean)cbo_tipoDocumento.getSelectedItem()).getTipoDocumentoIdentidadId();
        String numeroDocumentoIdentidad = ace_nroDocumento.getText().toString();

        if (celular.isEmpty()) {
            Toast.makeText(this, "Debe ingresar celular", Toast.LENGTH_SHORT).show();
        }
        else if (celular.length() != 9) {
            Toast.makeText(this, "El celular debe tener 9 dígitos", Toast.LENGTH_SHORT).show();
        }
        else if (numeroDocumentoIdentidad.trim().isEmpty()) {
            Toast.makeText(this, "Debe ingresar número de documento", Toast.LENGTH_SHORT).show();
        }
        else if (tipoDocumentoIdentidadId == Constants.TIPO_DOCUMENTO_IDENT_DNI && numeroDocumentoIdentidad.length() != 8) {
            Toast.makeText(this, "El número de documento debe tener 8 dígitos", Toast.LENGTH_SHORT).show();
        }
        else if (tipoDocumentoIdentidadId == Constants.TIPO_DOCUMENTO_IDENT_CE && numeroDocumentoIdentidad.length() != 12) {
            Toast.makeText(this, "El número de documento debe tener 12 caracteres", Toast.LENGTH_SHORT).show();
        }
        else if (tipoDocumentoIdentidadId == Constants.TIPO_DOCUMENTO_IDENT_PASS && numeroDocumentoIdentidad.length() != 12) {
            Toast.makeText(this, "El número de documento debe tener 12 caracteres", Toast.LENGTH_SHORT).show();
        }
        else {
            Intent i = new Intent(context, MensajeActivity.class);
            startActivity(i);
        }
    }
}
