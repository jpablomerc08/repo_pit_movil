package com.solucionesmoviles.appcovid19.di;

import com.solucionesmoviles.appcovid19.MainActivity;
import com.solucionesmoviles.appcovid19.MensajeActivity;
import com.solucionesmoviles.appcovid19.RegistroActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = RetrofitModule.class)
public interface RetrofitComponent {
    void inject(RegistroActivity registroActivity);
    void inject(MensajeActivity mensajeActivity);
    void inject(MainActivity mainActivity);
}
