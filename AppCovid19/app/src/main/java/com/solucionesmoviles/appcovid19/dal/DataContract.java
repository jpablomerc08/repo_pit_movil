package com.solucionesmoviles.appcovid19.dal;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

public class DataContract {
    public static final String CONTENT_AUTHORITY = "com.solucionesmoviles.appcovid19";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_PAIS = "pais";
    public static final String PATH_DEPARTAMENTO = "departamento";
    public static final String PATH_PROVINCIA = "provincia";
    public static final String PATH_DISTRITO = "distrito";
    public static final String PATH_TIPODOCUMENTOIDENTIDAD = "tipodocumentoidentidad";
    public static final String PATH_CIUDADANO = "ciudadano";
    public static final String PATH_CONTACTO = "contacto";
    public static final String PATH_DIRECCION = "direecion";

    public static final class PaisEntry implements BaseColumns {

        // Content URI para acceder a los datos del provider
        public static final  Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_PAIS);

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_URI + "/" + PATH_PAIS;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_URI + "/" + PATH_PAIS;

        // Nombre de la tabla
        public final static String TABLE_NAME = "Pais";

        // Columnas
        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_CODIGOISO = "codigoISO";
        public static final String COLUMN_NOMBRE = "nombre";

        public static Uri builUrl(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class DepartamentoEntry {

        // Content URI para acceder a los datos del provider
        public static final  Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_DEPARTAMENTO);

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_URI + "/" + PATH_DEPARTAMENTO;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_URI + "/" + PATH_DEPARTAMENTO;

        // Nombre de la tabla
        public final static String TABLE_NAME = "Departamento";

        // Columnas
        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_UBIGEO = "ubigeo";
        public static final String COLUMN_NOMBRE = "nombre";

        public static Uri builUrl(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class ProvinciaEntry {

        // Content URI para acceder a los datos del provider
        public static final  Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_PROVINCIA);

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_URI + "/" + PATH_PROVINCIA;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_URI + "/" + PATH_PROVINCIA;

        // Nombre de la tabla
        public final static String TABLE_NAME = "Provincia";

        // Columnas
        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_UBIGEO = "ubigeo";
        public static final String COLUMN_NOMBRE = "nombre";
        public static final String COLUMN_DEPARTAMENTOID = "departamentoId";

        public static Uri builUrl(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class DistritoEntry {

        // Content URI para acceder a los datos del provider
        public static final  Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_DISTRITO);

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_URI + "/" + PATH_DISTRITO;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_URI + "/" + PATH_DISTRITO;

        // Nombre de la tabla
        public final static String TABLE_NAME = "Distrito";

        // Columnas
        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_UBIGEO = "ubigeo";
        public static final String COLUMN_NOMBRE = "nombre";
        public static final String COLUMN_PROVINCIAID = "provinciaId";

        public static Uri builUrl(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class TipoDocumentoIdentidadEntry {

        // Content URI para acceder a los datos del provider
        public static final  Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_TIPODOCUMENTOIDENTIDAD);

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_URI + "/" + PATH_TIPODOCUMENTOIDENTIDAD;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_URI + "/" + PATH_TIPODOCUMENTOIDENTIDAD;

        // Nombre de la tabla
        public final static String TABLE_NAME = "TipoDocumentoIdentidad";

        // Columnas
        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_DESCRIPCIONLARGA = "descripcionLarga";
        public static final String COLUMN_DESCRIPCIONCORTA = "descripcionCorta";

        public static Uri builUrl(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final  class CiudadanoEntry {

        // Content URI para acceder a los datos del provider
        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_CIUDADANO);

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_URI + "/" + PATH_CIUDADANO;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_URI + "/" + PATH_CIUDADANO;

        // Nombre de la tabla
        public final static String TABLE_NAME = "Ciudadano";

        // Columnas
        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_APELLIDO_PATERNO = "apellidoPaterno";
        public static final String COLUMN_APELLIDO_MATERNO = "apellidoMaterno";
        public static final String COLUMN_NOMBRE = "nombre";
        public static final String COLUMN_SEXO = "sexo";
        public static final String COLUMN_NACIONALIDADID = "nacionalidadId";
        public static final String COLUMN_TIPODOCUMENTOIDETIDADID = "tipoDocumentoIdentidadId";
        public static final String COLUMN_NRODOCUMENTOIDENTIDAD = "nroDocumentoIdentidad";
        public static final String COLUMN_CELULAR = "celular";
        public static final String COLUMN_CORREO = "correo";
        public static final String COLUMN_FECHANACIMIENTO = "fechaNacimiento";

        public static Uri builUrl(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class ContactoEntry {
        // Content URI para acceder a los datos del provider
        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_CONTACTO);

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_URI + "/" + PATH_CONTACTO;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_URI + "/" + PATH_CONTACTO;

        // Nombre de la tabla
        public final static String TABLE_NAME = "Contacto";

        // Columnas
        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_CIUDADANOID = "ciudadanoId";
        public static final String COLUMN_NOMBRECONTACTO = "nombreContacto";
        public static final String COLUMN_PARENTESCO = "parentesco";
        public static final String COLUMN_TELEFONO = "telefono";
        public static final String COLUMN_CORREO = "correo";

        public static Uri builUrl(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class DireccionEntry {
        // Content URI para acceder a los datos del provider
        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_DIRECCION);

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_URI + "/" + PATH_DIRECCION;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_URI + "/" + PATH_DIRECCION;

        // Nombre de la tabla
        public final static String TABLE_NAME = "Direccion";

        // Columnas
        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_DISTRITOID = "distritoId";
        public static final String COLUMN_DESCRIPCION = "descripcion";
        public static final String COLUMN_CIUDADANOID = "ciudadanoId";

        public static Uri builUrl(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }
}
