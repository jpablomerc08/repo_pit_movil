package com.solucionesmoviles.appcovid19.models;

import java.io.Serializable;
import java.util.Date;

public class CiudadanoBean implements Serializable {
    private int ciudadanoId;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String nombre;
    private boolean sexo;
    private int nacionalidadId;
    private int tipoDocumentoIdentidadId;
    private String nroDocumentoIdentidad;
    private String celular;
    private String correo;
    private Date fechaNacimiento;

    private ContactoBean contacto;
    private DireccionBean direccion;

    public int getCiudadanoId() {
        return ciudadanoId;
    }

    public void setCiudadanoId(int ciudadanoId) {
        this.ciudadanoId = ciudadanoId;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isSexo() {
        return sexo;
    }

    public void setSexo(boolean sexo) {
        this.sexo = sexo;
    }

    public int getNacionalidadId() {
        return nacionalidadId;
    }

    public void setNacionalidadId(int nacionalidadId) {
        this.nacionalidadId = nacionalidadId;
    }

    public int getTipoDocumentoIdentidadId() {
        return tipoDocumentoIdentidadId;
    }

    public void setTipoDocumentoIdentidadId(int tipoDocumentoIdentidadId) {
        this.tipoDocumentoIdentidadId = tipoDocumentoIdentidadId;
    }

    public String getNroDocumentoIdentidad() {
        return nroDocumentoIdentidad;
    }

    public void setNroDocumentoIdentidad(String nroDocumentoIdentidad) {
        this.nroDocumentoIdentidad = nroDocumentoIdentidad;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public ContactoBean getContacto() {
        return contacto;
    }

    public void setContacto(ContactoBean contacto) {
        this.contacto = contacto;
    }

    public DireccionBean getDireccion() {
        return direccion;
    }

    public void setDireccion(DireccionBean direccion) {
        this.direccion = direccion;
    }
}
