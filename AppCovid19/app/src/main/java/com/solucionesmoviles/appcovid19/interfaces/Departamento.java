package com.solucionesmoviles.appcovid19.interfaces;

import com.solucionesmoviles.appcovid19.models.DepartamentoBean;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;

public interface Departamento {
    @GET("Departamento/listar")
    Observable<DepartamentoBean> listar();
}
